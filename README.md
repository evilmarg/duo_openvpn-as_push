# Duo OpenVPN-AS Push

I was using Duo's plugin for OpenVPN Access Server but was annoyed that it didn't support push notifications, so I added them. In the end, all it took was a few small modifications to Duo's plugin script (downloaded from here: https://github.com/duosecurity/duo_openvpn_as).

The original script caused OpenVPN to present a pop-up asking for a 6-digit Duo-generated code. My modified version does the same thing, but if you leave the field blank and just click "Sign In" instead it sends a push notification to your default device. I marked all of my changes with comments.

Hopefully this saves some effort for those of you who were as annoyed by the original behavior as I was. Enjoy!